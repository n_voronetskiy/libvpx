#pragma once


#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#ifdef rewind
#undef rewind
#endif // rewind
	
#ifdef fseek
#undef fseek
#endif // fseek
	
#ifdef fseeko
#undef fseeko
#endif // fseeko
	
#ifdef ftell
#undef ftell
#endif // ftell
	
#ifdef ftello
#undef ftello
#endif // ftello
	
#ifdef fclose
#undef fclose
#endif // fclose

#ifdef fread
#undef fread
#endif // fread

#ifdef fwrite
#undef fwrite
#endif // fwrite
	
#ifdef fopen
#undef fopen
#endif // fopen
	
#ifdef fflush
#undef fflush
#endif // fflush

#ifdef FILE
#undef FILE
#endif // FILE

#ifdef fprintf
#undef fprintf
#endif // fprintf

#ifdef vfprintf
#undef vfprintf
#endif // vfprintf

#ifdef stdin
#undef stdin
#endif // stdin

#ifdef stdout
#undef stdout
#endif // stdout

#ifdef stderr
#undef stderr
#endif // stderr

#ifdef ferror
#undef ferror
#endif // ferror

#ifdef feof
#undef feof
#endif // feof

#ifdef clearerr
#undef clearerr
#endif // clearerr

//#ifdef _setmode
//#undef _setmode
//#endif // _setmode

#ifdef _fileno
#undef _fileno
#endif // _fileno

#ifdef fileno
#undef fileno
#endif // fileno

#ifdef EOF
#undef EOF
#endif // EOF


#define rewind(file) __virtfseek__(file, 0, SEEK_SET)
#define fseek __virtfseek__
#define fseeko __virtfseek__
#define ftell __virtftell__
#define ftello __virtftell__
#define fclose __virtfclose__
#define fread __virtfread__
#define fwrite __virtfwrite__
#define fopen __virtfopen__
#define fflush __virtfflush__

#define ferror __virtferror__
#define feof __virtfeof__
#define clearerr __virtclearerr__

//#define _setmode __virtsetmode__
#define _fileno __virtfileno__
#define fileno __virtfileno__

#define FILE VIRTFILE
#define fprintf __virtfprintf__
#define vfprintf __virtvfprintf__

#define stdin  (__virtspecdesc__(0))
#define stdout (__virtspecdesc__(1))
#define stderr (__virtspecdesc__(2))

extern const int EOF;

    struct VIRTFILE_;

    typedef struct VIRTFILE_ VIRTFILE;

    int __virtfseek__(FILE* descriptor, size_t offset, int origin);

    size_t __virtftell__(FILE* descriptor);

    int __virtfclose__(FILE* descriptor);

    size_t __virtfwrite__(const void* ptr, size_t size, size_t count, FILE* descriptor);

    size_t __virtfread__(void* ptr, size_t size, size_t count, FILE* descriptor);

    FILE* __virtfopen__(const char* filename, const char* mode);

    int __virtfflush__(FILE * descriptor);

    int __virtferror__(FILE * descriptor);
    int __virtfeof__(FILE * descriptor);
    void __virtclearerr__(FILE * descriptor);
    int __virtsetmode__(int _FileHandle, int _Mode);

    int __virtfileno__(FILE * descriptor);

    size_t __virtfprintf__(FILE* descriptor, const char* format, ...);

    size_t __virtvfprintf__(FILE * descriptor, const char * format, va_list arg);


    FILE* __virtspecdesc__(const int num);

#ifdef __cplusplus
}
#endif
