
#include <list>
#include <algorithm>
#include <cstdio>
#include <cstdarg>
#include <cstring>

#include <virtbackdoor.hpp>
#include <virtio.h>

const int EOF = std::char_traits<char>::eof();

thread_local virtio::FileSystemDescriptors* virtio::fileSystemDescriptorsPtr = nullptr;


int __virtfseek__(FILE* descriptor, size_t offset, int origin)
{
    if (descriptor == nullptr)
    {
        return 1;
    }

    std::uint64_t bufferSize = 0;

    {
        std::unique_lock<std::mutex> lock(descriptor->fsIter->second.mutex);
        bufferSize = descriptor->fsIter->second.data.size();
    }

    int error = 0;

    switch (origin)
    {
    case SEEK_CUR:
    {
        descriptor->offset += offset;
        break;
    }
    case SEEK_END:
    {
        descriptor->offset = bufferSize + offset;
        break;
    }

    case SEEK_SET:
    {
        descriptor->offset = std::min(offset, bufferSize);
        break;
    }
    default:
    {
        error = 1;
        break;
    }
    }

    if (descriptor->offset > bufferSize)
    {
        descriptor->offset = bufferSize;
        error = 1;
    }

    descriptor->eof = false;

    return error;
}

size_t __virtftell__(FILE* descriptor)
{
    if (descriptor == nullptr)
    {
        return 0;
    }

    return descriptor->offset;
}

int __virtfclose__(FILE* descriptor)
{
    int result = EOF;

    if ((virtio::fileSystemDescriptorsPtr != nullptr)
        && (virtio::fileSystemDescriptorsPtr->closeDescriptor(descriptor) == true))
    {
        result = 0;
    }
    return result;
}

size_t __virtfwrite__(const void* ptr, size_t size, size_t count, FILE* descriptor)
{
    if (descriptor == nullptr)
    {
        return 0;
    }

    std::uint64_t writeSize = size * count;
    std::uint64_t writeEnd = descriptor->offset + writeSize;

    {
        std::lock_guard<std::mutex> lock(descriptor->fsIter->second.mutex);
        std::vector<char>& buffer = descriptor->fsIter->second.data;

        if (writeEnd > buffer.size())
        {
            buffer.resize(writeEnd);
        }

        std::memcpy(&(buffer[descriptor->offset]), (char*)ptr, writeSize);
    }
    descriptor->fsIter->second.conditionVariable.notify_all();

    descriptor->offset = writeEnd;
    return writeSize;
}

size_t __virtfread__(void* ptr, size_t size, size_t count, FILE* descriptor)
{
    if (descriptor == nullptr)
    {
        return 0;
    }

    std::uint64_t needed = size * count;

    std::unique_lock<std::mutex> lock(descriptor->fsIter->second.mutex);

    std::vector<char>& buffer = descriptor->fsIter->second.data;

/////////////////////////////////////////
    
    while (descriptor->fsIter->second.ongoing && ((buffer.size() - descriptor->offset) < needed))
    {
        descriptor->fsIter->second.conditionVariable.wait(lock);
    } 

    std::uint64_t available = std::min(needed, buffer.size() - descriptor->offset);

/////////////////////////////////////////

    if (available)
    {
        std::memcpy((char*)ptr, &(buffer[descriptor->offset]), available);
        descriptor->offset += available;
    }

    descriptor->eof = (descriptor->fsIter->second.ongoing == false && descriptor->offset == buffer.size());

    return available;
}

std::vector<char> virtio::readAll(FILE* descriptor)
{
    if (descriptor == nullptr)
    {
        return {};
    }

    return descriptor->fsIter->second.data;
}

void virtio::writeAll(FILE* descriptor, const std::vector<char>& buffer)
{
    __virtfwrite__(buffer.data(), 1, buffer.size(), descriptor);
}

FILE* __virtfopen__(const char* fileName, const char* mode)
{
    FILE* result = nullptr;

    if (virtio::fileSystemDescriptorsPtr != nullptr)
    {
        result = virtio::fileSystemDescriptorsPtr->openDescriptor(fileName, mode);
    }
    return result;
}

int __virtfflush__(FILE* descriptor)
{
    if (descriptor == nullptr)
    {
        return EOF;
    }

    return 0;
}

int __virtferror__(FILE * descriptor)
{
    return 0;
}

int __virtfeof__(FILE * descriptor)
{
    if (descriptor == nullptr)
    {
        return 0;
    }

    return descriptor->eof;
}

void __virtclearerr__(FILE * descriptor)
{
    if (descriptor == nullptr)
    {
        return;
    }

    descriptor->eof = false;
    descriptor->error = false;
}

int __virtsetmode__(int _FileHandle, int _Mode)
{
    return 0;
}

int __virtfileno__(FILE * descriptor)
{
    if (descriptor == nullptr)
    {
        return EOF;
    }

    return descriptor->id;
}

size_t __virtfprintf__(FILE* descriptor, const char* format, ...)
{
    va_list args;
    va_start(args, format);
    size_t result = __virtvfprintf__(descriptor, format, args);
    va_end(args);

    return result;
}

size_t __virtvfprintf__(FILE * descriptor, const char * format, va_list arg)
{
    const std::uint64_t bufferSize = 2048;
    char buffer[bufferSize];

    int count = vsnprintf(buffer, bufferSize, format, arg);

    return __virtfwrite__(buffer, 1, count, descriptor);
}

FILE* __virtspecdesc__(const int num)
{
    FILE* result = nullptr;

    if (virtio::fileSystemDescriptorsPtr != nullptr)
    {
        result = virtio::fileSystemDescriptorsPtr->getByNum(num);
    }
    return result;
}

