#pragma once

#include <string>
#include <map>
#include <vector>
#include <array>
#include <list>
#include <cstdint>
#include <mutex>
#include <memory>
#include <utility>
#include <condition_variable>

#include <virtio.h>

namespace virtio
{

    struct BlockingFile
    {
    public:
        BlockingFile(bool ongoing)
            : ongoing{ ongoing }
        {
        }

        std::vector<char> data;
        std::mutex mutex;
        std::condition_variable conditionVariable;

        bool ongoing;
    };


    // TODO: ???
    class OpenMode
    {
    public:
        OpenMode(const char* modeStr)
            : OpenMode(std::string(modeStr))
        {
        }

        OpenMode(const std::string modeStr)
        {
            for (const auto& symbol : modeStr)
            {
                switch (symbol)
                {
                case 'r':
                {
                    r = true;
                    break;
                }
                case 'b':
                {
                    b = true;
                    break;
                }
                case 'p':
                {
                    p = true;
                    break;
                }
                case 'x':
                {
                    x = true;
                    break;
                }
                case 'w':
                {
                    w = true;
                    break;
                }
                case 'o':
                {
                    o = true;
                    break;
                }
                case 'a':
                {
                    a = true;
                    break;
                }
                }
            }
        }

        bool r = false;
        bool w = false;
        bool b = false;
        bool p = false;
        bool x = false;
        bool o = false;
        bool a = false;
    };


    class FileSystem
    {
        using MapType = std::map<std::string, BlockingFile>;

    public:
        using iterator = typename MapType::iterator;

        iterator getFileIter(const char* fileName)
        {
            return std::find_if(std::begin(fileMap), std::end(fileMap), [fileName](const auto& file)
            {
                return file.first == fileName;
            });
        }

        bool fileExists(iterator fileIter)
        {
            return fileIter != std::end(fileMap);
        }

        iterator createFile(const char* fileName, bool ongoing)
        {
            return fileMap.emplace(std::piecewise_construct
                                   , std::make_tuple(fileName)
                                   , std::make_tuple(ongoing)).first;
        }

    private:
        MapType fileMap;
    };

} // namespace virtio

struct VIRTFILE_
{
public:

    VIRTFILE_(virtio::OpenMode mode, virtio::FileSystem::iterator fsIter, int id)
        : id{ id }
        , mode{ mode }
        , fsIter{ fsIter }
        , offset{ 0 }
        , error{ false }
        , eof{ false }
    {
    }

    int id;
    virtio::OpenMode mode;
    virtio::FileSystem::iterator fsIter;
    std::uint64_t offset;

    bool error;
    bool eof;

private:
};

inline bool operator==(const VIRTFILE_& lhs, const VIRTFILE_& rhs)
{
    return lhs.id == rhs.id;
}

namespace virtio
{

    class FileSystemDescriptors
    {
        constexpr static int systemDescriptorsNum = 3;
        constexpr static char* const systemDescriptorNames[systemDescriptorsNum] = { "__virtstdinname__"
                                                                                   , "__virtstdoutname__"
                                                                                   , "__virtstderrname__" };

    public:
        FileSystemDescriptors()
            : idCounter{ 0 }
        {
            for (auto descriptorName : systemDescriptorNames)
            {
                VIRTFILE_* descriptorPtr = openDescriptor(descriptorName, "a+");
                systemDecriptors[descriptorPtr->id] = descriptorPtr;
            }
        }

        VIRTFILE_* openDescriptor(const char* fileName, OpenMode mode)
        {
            bool error = false;

            auto it = fileSystem.getFileIter(fileName);

            if (fileSystem.fileExists(it) == false)
            {
                if (mode.r)
                {
                    error = true;
                }
                else
                {
                    it = fileSystem.createFile(fileName, mode.o);
                }
            }

            FILE* result = nullptr;

            if (error == false)
            {
                result = createDescriptor(mode, it);
            }
            return result;
        }


        bool closeDescriptor(VIRTFILE_* descriptor)
        {
            bool result = false;

            if (descriptor == nullptr)
            {
                return result;
            }

            std::lock_guard<std::mutex> lock(fileDescriptorsMutex);

            auto it = std::find(std::begin(fileDescriptors), std::end(fileDescriptors), *descriptor);

            if (it != std::end(fileDescriptors))
            {
                if (it->mode.o == true)
                {
                    {
                        std::lock_guard<std::mutex> lock(it->fsIter->second.mutex);
                        it->fsIter->second.ongoing = false;
                    }
                    it->fsIter->second.conditionVariable.notify_all();
                }

                fileDescriptors.erase(it);
                result = true;
            }

            return result;
        }

        VIRTFILE_* getByNum(const int num)
        {
            FILE* resultptr = nullptr;

            if (num < systemDescriptorsNum)
            {
                resultptr = systemDecriptors[num];
            }

            return resultptr;
        }

    private:

        VIRTFILE_ * createDescriptor(OpenMode mode, FileSystem::iterator fileIter)
        {
            std::lock_guard<std::mutex> lock(fileDescriptorsMutex);
            fileDescriptors.emplace_back(mode, fileIter, idCounter++);
            return &(fileDescriptors.back());
        }


        FileSystem fileSystem;

        std::list<VIRTFILE_> fileDescriptors;
        std::array<VIRTFILE_*, systemDescriptorsNum> systemDecriptors;

        std::mutex fileDescriptorsMutex;

        int idCounter;
    };


    std::vector<char> readAll(FILE* descriptor);
    void writeAll(FILE* descriptor, const std::vector<char>& buffer);

    extern thread_local FileSystemDescriptors* fileSystemDescriptorsPtr;

} // namespace virtio


