
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <future>

#include <vpxenc.h>
#include <virtbackdoor.hpp>


namespace ffvpx
{

    class encoder
    {
        const char* tempFileName = "temp_file_name.yuv";
        const char* resultFileName = "result_file_name.webm";

    public:
        encoder(std::vector<const char*> codecArgs)
            : allArgs{ tempFileName, "-o", resultFileName }
        {
            inputFile = fileSystemDescriptors.openDescriptor(tempFileName, "wo");

            allArgs.insert(std::end(allArgs), std::begin(codecArgs), std::end(codecArgs));

            encoderTask = std::async(std::launch::async , [this]()
            {
                virtio::fileSystemDescriptorsPtr = &fileSystemDescriptors;
                return ffvpxenc(allArgs.size(), allArgs.data());
            });
        }

        void pushData(const std::vector<char>& data)
        {
            virtio::writeAll(inputFile, data);
        }

        std::vector<char> getResult()
        {
            std::vector<char> result;
            int returnCode = 1;

            fileSystemDescriptors.closeDescriptor(inputFile);

            if (encoderTask.valid())
            {
                returnCode = encoderTask.get();
            }
            
            FILE* resultFile = fileSystemDescriptors.openDescriptor(resultFileName, "r");
            result = virtio::readAll(resultFile);

            return result;
        }

    private:
        FILE* inputFile;
        std::future<int> encoderTask;
        std::vector<const char*> allArgs;
        virtio::FileSystemDescriptors fileSystemDescriptors;
    };

}


int main(void)
{
    std::ifstream input("output_small.yuv", std::ios::binary);

    auto const startPos = input.tellg();
    input.ignore(std::numeric_limits<std::streamsize>::max());
    std::streamsize const fileSize = input.gcount();
    input.seekg(startPos);

    std::vector<char> inputBuffer(fileSize);

    input.read(inputBuffer.data(), fileSize);

    std::vector<const char*> codecArgs{ "--codec=vp9", "-w", "640", "-h", "480", "-p", "2"
                                        , "-t", "4", "--best", "--yv12" };

    ffvpx::encoder encoder(codecArgs);

    encoder.pushData(inputBuffer);

    std::vector<char> resultBuffer = encoder.getResult();

    std::ofstream output("result.webm", std::ios::out | std::ios::binary);
    output.write(resultBuffer.data(), resultBuffer.size());

}
