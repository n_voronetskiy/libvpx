// Copyright (c) 2012 The WebM project authors. All Rights Reserved.
//
// Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file in the root of the source
// tree. An additional intellectual property rights grant can be found
// in the file PATENTS.  All contributing project authors may
// be found in the AUTHORS file in the root of the source tree.

#include "mkvmuxer/mkvwriter.h"

#include <sys/types.h>

#ifdef _MSC_VER
#include <share.h>  // for _SH_DENYWR
#endif

#include <virtio.h>

namespace mkvmuxer {

MkvWriter::MkvWriter() : file_(NULL), writer_owns_file_(true) {}

MkvWriter::MkvWriter(FILE* fp) : file_(fp), writer_owns_file_(false) {}

MkvWriter::~MkvWriter() { Close(); }

int32 MkvWriter::Write(const void* buffer, uint32 length) {
  if (!file_)
    return -1;

  if (length == 0)
    return 0;

  if (buffer == NULL)
    return -1;

  const size_t bytes_written = fwrite(buffer, 1, length, file_);

  return (bytes_written == length) ? 0 : -1;
}

bool MkvWriter::Open(const char* filename) {
  if (filename == NULL)
    return false;

  if (file_)
    return false;

  file_ = fopen(filename, "wb");

  if (file_ == NULL)
    return false;
  return true;
}

void MkvWriter::Close() {
  if (file_ && writer_owns_file_) {
    fclose(file_);
  }
  file_ = NULL;
}

int64 MkvWriter::Position() const {
  if (!file_)
    return 0;

  return ftell(file_);
}

int32 MkvWriter::Position(int64 position) {
  if (!file_)
    return -1;

  return fseek(file_, position, SEEK_SET);
}

bool MkvWriter::Seekable() const { return true; }

void MkvWriter::ElementStartNotify(uint64, int64) {}

}  // namespace mkvmuxer
